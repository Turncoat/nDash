/*
 * nDash - A simple dashboard for Assetto Corsa, for external displays or other
 *         systems on the network.
 ******************************************************************************
 * Copyright (c) 2014 Anthony Goins
 *
 */

#define ARDUINO_SOURCE 1

#include "Arduino.h"
#include "rs232.h"
#include "../kernel/Kernel.h"

using namespace std;

namespace nDash {
    class Arduino : public GraphicsServer::GraphicsDriver {
        private: uint8_t        com_port;
        private: uint32_t       bdrate = 2000000;

        public: ARDUINO_API virtual const std::string &pluginInfo() const {
            static string info("Arduino Serial USB communication");
            return info;
        }

        public: ARDUINO_API virtual ~Arduino() {}

        public: ARDUINO_API virtual const std::string &getName() const {
            static string sName("Arduino");
            return sName;
        }

        unique_ptr<Renderer> createRenderer() {
            uint8_t     cport   = 0;
            char        mode[]  = {'8', 'N', '1', 0};

            for (cport = 0; cport < 16; cport++) {
                if (!RS232_OpenComport(cport, bdrate, mode, 0)) {
                    cout << "Found comport: " << (cport+1) << endl;
                    set_comport(cport+1);
                    break;
                } else if (cport == 16) 
                    break;
                else
                    continue;
            }
            return unique_ptr<Renderer>(new Renderer());
        }

        public: ARDUINO_API virtual void set_comport(uint8_t port) {
                    if (port < 0 || port > 15)
                        return;
                    com_port = port;
                }

        public: ARDUINO_API virtual int sendUpdate(uint8_t type, const char *data, ...) {
                    char buf[1024+1];

                    va_list args;
                    va_start(args, data);
                    vsnprintf(buf, sizeof buf, data, args);
                    va_end(args);

                    RS232_cputs(com_port, buf);
                    return 0;
                }
    };

    extern "C" ARDUINO_API int getEngineVersion() {
        return 1;
    }

    extern "C" ARDUINO_API void registerPlugin(Kernel &kernel) {
        kernel.getGraphicsServer().addGraphicsDriver(unique_ptr<GraphicsServer::GraphicsDriver>(new Arduino()));
    }
}
